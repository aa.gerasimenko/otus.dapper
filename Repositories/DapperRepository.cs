﻿using Dapper;
using Npgsql;
using otus.dapper.Models;
using System;
using System.Collections.Generic;

namespace otus.dapper.Repositories
{
    public class DapperRepository
    {
        public static IEnumerable<Student> GetStudents()
        {
            using (var conn = new NpgsqlConnection(Config.SqlConnectionString))
            {
                string sql = @"select id, email, name from student";
                return conn.Query<Student>(sql);
            }
        }

        public static IEnumerable<Student> GetStudents(string pattern)
        {
            using (var conn = new NpgsqlConnection(Config.SqlConnectionString))
            {
                //string sql = $"select id, email, name from student where name like '%" + pattern + "%'";
                string sql = $"select id, email, name from student where name like '%@pattern%'";
                return conn.Query<Student>(sql, new { pattern });
            }
        }

        public static Student GetStudentById(string id)
        {
            using (var conn = new NpgsqlConnection(Config.SqlConnectionString))
            {
                string sql = $"select id, email, name from student where id = @id";
                return conn.QueryFirstOrDefault<Student>(sql, new { id });
            }
        }

        internal static void Insert(Student student)
        {
            using (var conn = new NpgsqlConnection(Config.SqlConnectionString))
            {
                string sql = $"insert into student (id, email, name) values (@id, @email,@name)";
                conn.Execute(sql, new { id = student.Id, name = student.Name, email = student.Email });
            }
        }
    }
}