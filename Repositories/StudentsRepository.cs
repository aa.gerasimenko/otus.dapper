﻿using Dapper;
using Npgsql;
using otus.dapper.Models;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace otus.dapper.Repositories
{
    internal class StudentsRepository
    {
        public List<Student> GetStudents()
        {
            var query = @" select
                id, name, email
                from student";

            using (var connection = new NpgsqlConnection(Config.SqlConnectionString))
            {
                var list = connection.Query<Student>(query);
                return list.ToList();
            }
        }

        public Student GetStudent(string studentId)
        {
            var query = @" select
                id, name, email
                from student
                where id=@id";

            using (var connection = new NpgsqlConnection(Config.SqlConnectionString))
            {
                //var s = connection.Query<Student>(query, new { id=studentId }).FirstOrDefault();
                var s = connection.QueryFirstOrDefault<Student>(query, new { id = studentId });
                return s;
            }
        }
    }
}