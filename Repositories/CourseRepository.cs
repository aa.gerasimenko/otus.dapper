﻿using Dapper;
using Npgsql;
using otus.dapper.Models;
using System.Collections.Generic;
using System.Linq;

namespace otus.dapper.Repositories
{
    internal class CourseRepository
    {
        public List<Course> GetCoursesInfo()
        {
            var query = @" select
                --student.id as id, student.name, student.email, course.name, course.id as cid
                *
                from student
                    inner join studentcourse on student.id=studentcourse.studentid
                    inner join course on course.id=studentcourse.courseid
                ";

            using (var connection = new NpgsqlConnection(Config.SqlConnectionString))
            {
                var list = connection.Query<Student, Course, Course>(query, (s, c) =>
                {
                    return c;
                });
                return list.ToList();
            }
        }
    }
}