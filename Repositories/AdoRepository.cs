﻿using Npgsql;
using otus.dapper.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace otus.dapper.Repositories
{
    internal class AdoRepository
    {
        public static List<Student> GetStudents()
        {
            var students = new List<Student>();
            using (var conn = new NpgsqlConnection(Config.SqlConnectionString))
            {
                string sql = @"select id, email as e, name from student";
                conn.Open();

                using (var command = new NpgsqlCommand(sql, conn))
                {
                    var reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        students.Add(new Student()
                        {
                            Id = reader["id"].ToString(),
                            Email = reader["name"].ToString(),
                            Name = reader["e"].ToString(),
                        });
                    }
                }
            }

            return students;
        }

    }
}
