﻿using LinqToDB;
using LinqToDB.Data;
using otus.dapper.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace otus.dapper.Repositories
{
    internal class LinqRepository
    {
        static DataConnection db = new LinqToDB.Data.DataConnection(ProviderName.PostgreSQL, Config.SqlConnectionString);
        public static List<StudentPoco> GetStudents(string pattern)
        {
                return db.GetTable<StudentPoco>()
                    .Where(x=>x.Name.Contains(pattern))
                    .OrderByDescending(x=>x.Name)
                    .ToList();
        }

        public static int CountStudents(string pattern)
        {
            return db.GetTable<StudentPoco>()
                    .Where(x => x.Name.Contains(pattern))
                    .OrderByDescending(x => x.Name)
                    .Count();
        }
    }
}
