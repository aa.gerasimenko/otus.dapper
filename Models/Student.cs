﻿namespace otus.dapper.Models
{
    public struct Student
    {
        public string Email { get; set; }

        public string Id { get; set; }

        public string Name { get; set; }
    }
}