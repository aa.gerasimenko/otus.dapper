﻿using System.Collections.Generic;

namespace otus.dapper.Models
{
    public class Course
    {
        public Course()
        {
            Students = new List<Student>();
        }

        public string Id { get; set; }

        public string Name { get; set; }

        public List<Student> Students { get; set; }
    }
}