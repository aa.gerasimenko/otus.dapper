﻿using LinqToDB.Mapping;

namespace otus.dapper.Models
{
    [Table(Name = "student")]
    public class StudentPoco
    {
        [Column(Name="email")]
        public string Email { get; set; }

        [PrimaryKey]
        [Column(Name="id")]
        public string Id { get; set; }

        [Column(Name="name")]
        public string Name { get; set; }
    }
}