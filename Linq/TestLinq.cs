﻿using otus.dapper.Models;
using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace otus.dapper.Linq
{
    public static class TestLinq
    {
        public static List<StudentPoco> GetStudents()
        {
            // order by name desc

            using (var db = new LinqToDB.Data.DataConnection(LinqToDB.ProviderName.PostgreSQL, Config.SqlConnectionString))
            {
                var table = db.GetTable<StudentPoco>();

                var list = table.Where(x=>x.Name.Contains("T"))
                    .OrderByDescending(x=> x.Name)
                    
                    //.Take(2)
                    .ToList();

                return list;
            }
        }

        public static int CountSudents()
        {
            using (var db = new LinqToDB.Data.DataConnection(LinqToDB.ProviderName.PostgreSQL, Config.SqlConnectionString))
            {
                var table = db.GetTable<StudentPoco>();

                var count = table//.Where(x => x.Name.Contains("T"))
                    //.Take(2)
                    .Max(x=>x.Name.Length);

                return count;
            }
        }
    }
}
