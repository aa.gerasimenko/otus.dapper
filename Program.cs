﻿using otus.dapper.Generators;
using otus.dapper.Linq;
using otus.dapper.Models;
using otus.dapper.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;

namespace otus.dapper
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            var students = LinqRepository.GetStudents("Sabryna");
            students.ForEach(PrintStudent);

            Console.WriteLine("WorldEnded!");
        }

        private static void Ado()
        {
            var students = AdoRepository.GetStudents();
            students.ForEach(PrintStudent);
        }

        private static void Dapper()
        {
            var students = DapperRepository.GetStudents("Sabryna");
            students.ForEach(PrintStudent);
        }

        private static void DapperId()
        {
            var student = DapperRepository.GetStudentById("3a924658-5add-4a4b-8dff-b29ec2ced7d91");
            PrintStudent(student);
        }

        private static void DapperInsert()
        {
            Student student = new Student
            {
                Id = "111",
                Email = "otus@otus.ru",
                Name = "otus"
            };
            DapperRepository.Insert(student);
        }

        private static void PrintStudent(Student student)
        {
            Console.WriteLine($"{student.Id} {student.Name} \t{student.Email}");
        }

        private static void PrintStudent(StudentPoco student)
        {
            Console.WriteLine($"{student.Id} {student.Name} \t{student.Email}");
        }

    }

    public static class LinqExt
    {
        public static void ForEach<T>(this IEnumerable<T> source, Action<T> action)
        {
            foreach (var item in source)
                action(item);
        }
    }
}