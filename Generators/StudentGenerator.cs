﻿using Dapper;
using Npgsql;
using otus.dapper.Models;
using System;

namespace otus.dapper.Generators
{
    public static class StudentGenerator
    {
        public static void Generate(int count)
        {
            using (var connection = new NpgsqlConnection(Config.SqlConnectionString))
            {
                while (count > 0)
                {
                    var student = new Student() { Id = Guid.NewGuid().ToString() };
                    student.Name = Faker.Name.FullName();
                    student.Email = Faker.Internet.Email(student.Name);

                    var query = @"insert into student (id, name, email)
                            values (@Id, @name, @email)";

                    var list = connection.Execute(query, new { Id = student.Id, name = student.Name, email = student.Email });

                    count--;
                }
            }
        }

        public static void GenerateС()
        {
            var query = @"insert into course (id, name)
                            values (@Id, @name)";

            using (var connection = new NpgsqlConnection(Config.SqlConnectionString))
            {
                connection.Execute(query, new { Id = "basic", name = "Basic c#" });
                connection.Execute(query, new { Id = "pro",   name = "c# professional" });
                connection.Execute(query, new { Id = "asp",   name = "aps.net" });
            }
        }
    }
}